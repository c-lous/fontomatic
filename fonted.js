/*
 * config: 
 * element: font editor element to attach to
 * cell_width: width of editor cell
 * cell_height: height of editor cell
 * font_width: width in pixels of font
 * font_height: width in pixels of font
 * onCharUpdate: function width current char code and data as args
 */
function FontEditor(config) {
  var self = this;
  
  self.config = config;
  
  self.element = $(config.element);
  
  var onUpdate = function() {
    if(self.updateTimer)
      clearTimeout(self.updateTimer);
    
    var newCharData = [];
    var di = 0;
    for(var y=0; y < self.config.font_height; y++) {
      for(var x=0; x < self.config.font_width; x++) {
	var cell = $(self.gridRoot).find("div[cell-x='"+x+"'][cell-y='"+y+"']");
	newCharData[di++] = cell.hasClass("active") ? 1 : 0;
      }
      
    }
    
    self.updateTimer = setTimeout(function() {
      self.config.onUpdate(self.charCode, newCharData);
    }, 125);
  }

  self.mouseDown = false;

  $("body")
      .on("mousedown", function() {
//	console.log("mosuedown");
	self.mouseDown = true;
      })
      .on("mouseup", function() {
//	console.log("mosueup");
	self.mouseDown = false;
      });

  var createGrid = function(data) {
    if(self.gridRoot)
      self.gridRoot.remove();
    var root = self.gridRoot = $("<div>").appendTo(self.element)
      .css("width", (self.config.font_width*self.config.cell_size+1) + "px")
      .css("border-left", "1px dashed white")
      .css("border-top", "1px dashed white")
    var di = 0;
    for(var y=0; y < self.config.font_height; y++) {
      var row = $("<div>")
	.attr("class", "cellrow")
	.appendTo(root);
      for(var x=0; x < self.config.font_width; x++) {
	var isSet = data[di++] == true;
	$("<div>").attr("cell-y", y).attr("cell-x",x).attr("class", "cell").appendTo(row)
	  .addClass(isSet?"active":"")
	  .css("width", self.config.cell_size)
	  .css("height", self.config.cell_size)
	  .on("mousedown", function() {
	    $(this).toggleClass("active");
	    onUpdate();
	  })
	  .on("mouseover", function(e) {
	    if(self.mouseDown) {
	      $(this).toggleClass("active");
	      onUpdate();
	    }
	    e.preventDefault();
	  });
      }
    }
  };

  function disableSelection(target){
    if (typeof target.onselectstart!="undefined") //IE 
      target.onselectstart=function(){return false;}
    else if (typeof target.style.MozUserSelect!="undefined") //Firefox 
      target.style.MozUserSelect="none"
    else //All other ie: Opera
      target.onmousedown=function(){return false;}
    target.style.cursor = "default"
  }
  
//  createGrid([]);
  
  return {
    editChar: function(charCode, charData, onUpdate) {
      self.charCode = charCode;
      self.charData = charData;
      self.onUpdate = onUpdate;
      createGrid(charData);
      disableSelection(document.body);
    },
    toggleGrid: function() {
      self.gridRoot.find(".cell").toggleClass("guide-x").toggleClass("guide-y");
    }
  };
}
